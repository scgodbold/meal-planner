import { defineStore } from 'pinia'
import router from '@/router'
import  AuthService from '@/services/auth'

export const useAuthStore = defineStore('auth', {
    state: () => ({
        user: JSON.parse(localStorage.getItem('user')),
        returnUrl: null
    }),
    getters: {
        isAuthenticated() {
            return this.user && this.user.token
        },
        accessToken() {
            return this.user.token
        },
        tokenExpiry() {
            var base64Url = this.user.token.split('.')[1]
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
            var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
            return JSON.parse(jsonPayload)['exp']
        },
        minValid() {
            var ms = (this.tokenExpiry * 1000) - Date.now()
            // Convert ms to min
            return ms / 1000 /60
        }
    },
    actions: {
        async login(username, password) {
            try {
                const user = await AuthService.login(username, password)
                this.user = user

                localStorage.setItem('user', JSON.stringify(user))
                router.push(this.returnUrl || '/')
            } catch (error) {
                // TODO: Display errors to user
                // for auth failures
            }
        },
        async refresh() {
            const user = await AuthService.refresh(this.user.token)
            localStorage.setItem('user', JSON.stringify(user))
        },
        logout() {
            this.user = null
            localStorage.removeItem('user')
            router.push('/login')
        }
    }
})
