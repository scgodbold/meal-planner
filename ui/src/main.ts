/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'


// Configure Routing
import router from './router'

// Build our App
const app = createApp(App)
registerPlugins(app)

// Send it
app.use(router).mount('#app')
