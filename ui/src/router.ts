import { createRouter, createWebHashHistory } from 'vue-router'
import { useAuthStore } from '@/stores/auth'

import Home from '@/views/Home.vue'
// import Recipes from '@/views/Recipes.vue'
// import Recipe from '@/views/Recipe.vue'
import Login from '@/views/Login.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            requiresAuth: true
        }
    },
    // {
    //     path: '/recipes',
    //     name: 'Recipes',
    //     component: Recipes,
    //     meta: {
    //         requiresAuth: true
    //     }
    // },
    // {
    //     path: '/recipe/:id',
    //     name: 'Recipe',
    //     component: Recipe,
    //     meta: {
    //         requiresAuth: true
    //     }
    // },
    {
        path: "/login",
        name: 'Login',
        component: Login,
        meta: {
            requiresAuth: false
        }
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

router.beforeEach(async (to) => {
    const authStore = useAuthStore()

    if (to.meta.requiresAuth && !authStore.user) {
        authStore.returnUrl = to.fullPath
        return '/login'
    }
})

export default router
