import axios from 'axios'

import { useAuthStore } from '@/stores/auth'

export function jwtInterceptor() {
    const authStore = useAuthStore()
    axios.interceptors.request.use(request => {
        // TODO: Check if its our API endpoint too
        if (authStore.isAuthenticated) {
            request.headers.common.Atuhorization = `Bearer ${authStore.accessToken}`;
        }
        return request
    })
}
