/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import { loadFonts } from './webfontloader'
import { jwtInterceptor } from './jwt-interceptor'
import { usePinia } from './pinia'
import vuetify from './vuetify'

// Types
import type { App } from 'vue'

export function registerPlugins (app: App) {
    usePinia(app)
    loadFonts()
    jwtInterceptor()
    app.use(vuetify)
}
