import { createPinia } from 'pinia'

export function usePinia(app: app) {
    const store = createPinia()
    app.use(store)
}
