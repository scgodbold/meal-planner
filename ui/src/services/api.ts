import axios from 'axios'

import { useAuthStore } from '@/stores/auth'

const api = axios.create({
    baseURL: import.meta.env.API_HOST
    timeout: 1000,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
})

// Check if logged in & set our bearer token
api.interceptors.request.use(request => {
    const authStore = useAuthStore()
    if (authStore.isAuthenticated) {
        request.headers.Authorization = `Bearer ${authStore.accessToken}`
    }
    return request
})

api.interceptors.response.use((response) => {
    const authStore = useAuthStore()
    if (authStore.minValid < 100) {
        authStore.refresh()
    }
    return response
}, (error) => {
    if ([401].includes(error.response.status)) {
        const authStore = useAuthStore()
        authStore.logout()
    }
})

export default api
