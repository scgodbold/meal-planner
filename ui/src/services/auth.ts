import axios from 'axios'

// Use its own axios API so we may call this from
// our API router for the rest of the app
const authApi = axios.create({
    baseURL: 'http://localhost:8080/auth',
    timeout: 1000,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
})

class AuthService {
    login(username, password) {
        return authApi.post('/login', {
            username: username,
            password: password,
        })
        .then(response => {
            return response.data
        })
    }
    refresh(token) {
        // Take token in since we dont use the auto-injected
        // method as this would create a dependency cycle
        return authApi.post('/refresh', {}, {
            headers: {
                "Authorization": `Bearer ${token}`,
            }
        }).then(response => {
            return response.data
        })
    }
}

export default new AuthService()
