package token

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

func GenerateToken(userId uint) (string, error) {
	signingSecret := os.Getenv("MP_TOKEN_SIGNING_SECRET")
	parsedDuration, err := time.ParseDuration(os.Getenv("MP_TOKEN_VALIDITY_DURATION"))
	if err != nil {
		// Default to 1 hour
		log.Println("[WARNING] Unable to parse MP_TOKEN_VALIDITY_DURATION, defaulting to 1 hour")
		parsedDuration = time.Duration(1 * time.Hour)
	}

	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["user_id"] = userId
	claims["exp"] = time.Now().Add(parsedDuration).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(signingSecret))
}

func TokenValid(tokenString string) error {
	_, err := ParseToken(tokenString)
	return err
}

func ExtractTokenID(tokenString string) (uint, error) {
	token, err := ParseToken(tokenString)
	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		uid, err := strconv.ParseUint(fmt.Sprintf("%.0f", claims["user_id"]), 10, 32)
		if err != nil {
			return 0, err
		}
		return uint(uid), nil
	}
	return 0, nil
}

func ParseToken(tokenString string) (*jwt.Token, error) {
	signingSecret := os.Getenv("MP_TOKEN_SIGNING_SECRET")
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(signingSecret), nil
	})
	return token, err
}

func TokenStringFromContext(c *gin.Context) string {
	bearerToken := c.Request.Header.Get("Authorization")
	if len(strings.Split(bearerToken, " ")) == 2 {
		return strings.Split(bearerToken, " ")[1]
	}
	return ""
}
