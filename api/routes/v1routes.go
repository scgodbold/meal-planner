package routes

import (
	"github.com/gin-gonic/gin"
	v1 "gitlab.com/scgodbold/meal-planner/api/controllers/v1"
	"gitlab.com/scgodbold/meal-planner/api/middlewares"
)

func RegisterV1Routes(r *gin.RouterGroup) {
    r.Use(middlewares.JwtAuthMiddleware())
    {
        r.PUT("/recipe", v1.CreateRecipe)
        r.GET("/recipes", v1.FetchAllRecipes)
        r.POST("/recipe/:id", middlewares.LoadRecipeMiddleware(), v1.UpdateRecipe)
        r.GET("/recipe/:id", middlewares.LoadRecipeMiddleware(), v1.FetchRecipe)
        r.DELETE("/recipe/:id", middlewares.LoadRecipeMiddleware(), v1.DeleteRecipe)
    }
}
