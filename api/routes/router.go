package routes

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/scgodbold/meal-planner/api/controllers/auth"
	"gitlab.com/scgodbold/meal-planner/api/middlewares"
)

func GetRouter() *gin.Engine {
	r := gin.Default()

	// Add Cors
	// TODO: Add cors configuration for allowed origins
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowOrigins = []string{"*"}
	corsConfig.AddAllowHeaders("Authorization")
	r.Use(cors.New(corsConfig))
    r.Use(middlewares.SetJwtUserMiddleware())
    r.Use(gin.Recovery())

	// Register Default Routes
	r.GET("/ping", HealthCheckHandler)
	r.POST("/auth/register", auth.Register)
	r.POST("/auth/login", auth.Login)
	r.POST("/auth/refresh", auth.Refresh)

    RegisterV1Routes(r.Group("/v1"))
	return r
}

func HealthCheckHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "pong",
	})
}
