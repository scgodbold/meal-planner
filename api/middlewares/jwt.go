package middlewares

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/scgodbold/meal-planner/api/util/token"
)

func JwtAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(token.TokenStringFromContext(c))
		if err != nil {
			c.String(http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			return
		}
		c.Next()
	}
}

func SetJwtUserMiddleware() gin.HandlerFunc {
    return func(c *gin.Context) {
		tokenstring := token.TokenStringFromContext(c)
        uid, err := token.ExtractTokenID(tokenstring)
        if err == nil {
            c.Set("uid", uid)
        }
        c.Next()
    }
}
