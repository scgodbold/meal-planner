package middlewares

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/scgodbold/meal-planner/api/controllers"
	"gitlab.com/scgodbold/meal-planner/api/services/recipe"
)

func LoadRecipeMiddleware() gin.HandlerFunc {
    return func(c *gin.Context) {
        uid := c.GetUint("uid")
        recipeId, err := strconv.ParseUint(c.Param("id"), 10, 32)
        if err != nil {
            // Bad Id passed, fail
            controllers.SendErrorResponse(c, http.StatusBadRequest, err)
            return
        }
        r, err := recipe.FetchById(uint(recipeId))
        if err != nil {
            // Unable to find recipe
            controllers.SendErrorResponse(c, http.StatusUnauthorized, err)
            c.Abort()
            return
        }
        // TODO: Move this to a bigger authorization middleware and out of a loader
        if r.Owner != uid {
            // Requestor doesnt own the recipe
            controllers.SendErrorResponse(c, http.StatusUnauthorized, nil)
            c.Abort()
            return
        }
        c.Set("recipe", r)
        c.Next()
    }
}
