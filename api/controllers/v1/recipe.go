package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/scgodbold/meal-planner/api/controllers"
	"gitlab.com/scgodbold/meal-planner/api/models"
	"gitlab.com/scgodbold/meal-planner/api/services/recipe"
)

type RecipeInput struct {
    Title string `json:"title" binding:"required"`
    Steps []string `json:"steps" binding:"required"`
    Ingredients []Ingredient `json:"ingredients" binding:"required"`
    Tags  []string `json:"tags"`

}

type Ingredient struct {
    Name        string `json:"name" binding:"required"`
    Amount      float32 `json:"amount" binding:"required"`
    Unit        string `json:"unit" binding:"required"`
}

type RecipeOutput struct {
    Id uint `json:"id" binding:"required"`
    Title string `json:"title" binding:"required"`
    Steps []string `json:"steps" binding:"required"`
    Ingredients []Ingredient `json:"ingredients" binding:"required"`
    Tags  []string `json:"tags"`
}

type RecipeUpdate struct {
    Title string `json:"title"`
    Steps []string `json:"steps"`
    Ingredients []Ingredient `json:"ingredients"`
    Tags  []string `json:"tags"`
}


func CreateRecipe(c *gin.Context) {
    var input RecipeInput
    uid := c.GetUint("uid")

    if err := c.ShouldBindJSON(&input); err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
        return
    }

    ingredients := []models.Ingredient{}
    for _, ind := range input.Ingredients {
        newIngredient, err := recipe.CreateIngredient(ind.Name, ind.Amount, ind.Unit)
        if err != nil {
            controllers.SendErrorResponse(c, http.StatusBadRequest, err)
            return
        }
        ingredients = append(ingredients, newIngredient)
    }
    _, err := recipe.Create(uid, input.Title, input.Steps, ingredients, input.Tags)
    if err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
        return
    }
    controllers.SendSuccessResponse(c, http.StatusCreated, gin.H{})
}

func FetchAllRecipes(c *gin.Context) {
    uid := c.GetUint("uid")
    recipes, err := recipe.FetchByOwner(uid)
    if err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
        return
    }
    response := []RecipeOutput{}
    for _, v := range recipes {
        response = append(response, mapRecipeToOutput(v))
    }
    controllers.SendSuccessResponse(c, http.StatusOK, response)
}

func FetchRecipe(c *gin.Context) {
    r, ok := c.Get("recipe")
    if !ok {
        controllers.SendErrorResponse(c, http.StatusBadRequest, nil)
        return
    }
    controllers.SendSuccessResponse(c, http.StatusOK, mapRecipeToOutput(r.(models.Recipe)))
}

func DeleteRecipe(c *gin.Context) {
    r, ok := c.Get("recipe")
    if !ok {
        controllers.SendErrorResponse(c, http.StatusBadRequest, nil)
        return
    }
    err := recipe.Delete(r.(models.Recipe).ID)
    if err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
        return
    }
    controllers.SendSuccessResponse(c, http.StatusNoContent, gin.H{})
}

func UpdateRecipe(c *gin.Context) {
    var update RecipeUpdate
    r, ok := c.Get("recipe")
    if !ok {
        controllers.SendErrorResponse(c, http.StatusBadRequest, nil)
        return
    }
    var ingredients []models.Ingredient
    if update.Ingredients != nil {
        ingredients = []models.Ingredient{}
        for _, ind := range update.Ingredients {
            newIngredient, err := recipe.CreateIngredient(ind.Name, ind.Amount, ind.Unit)
            if err != nil {
                controllers.SendErrorResponse(c, http.StatusBadRequest, err)
                return
            }
            ingredients = append(ingredients, newIngredient)
        }
    }

    if err := c.ShouldBindJSON(&update); err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
        return
    }
    err := recipe.Update(r.(models.Recipe).ID, update.Title, update.Steps, ingredients, update.Tags)
    if err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
        return
    }
    controllers.SendSuccessResponse(c, http.StatusOK, gin.H{})

}

func mapRecipeToOutput(recipe models.Recipe) RecipeOutput {
    ingredients := []Ingredient{}
    for _, v := range recipe.Ingredients {
        ingredients = append(ingredients, Ingredient{Name: v.Name, Amount: v.Amount, Unit: v.Unit})
    }
    return RecipeOutput{
        Id: recipe.ID,
        Title: recipe.Title,
        Steps: recipe.Steps,
        Ingredients: ingredients,
        Tags: recipe.Tags,
    }
}

