package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/scgodbold/meal-planner/api/controllers"
	"gitlab.com/scgodbold/meal-planner/api/services/user"
)

type RegisterInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func Register(c *gin.Context) {
	var input RegisterInput

	if err := c.ShouldBindJSON(&input); err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
		return
	}
    err := user.Register(input.Username, input.Password)
    if err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
        return
    }

	c.JSON(http.StatusCreated, gin.H{})
}
