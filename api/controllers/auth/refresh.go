package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/scgodbold/meal-planner/api/controllers"
	"gitlab.com/scgodbold/meal-planner/api/services/user"
	"gitlab.com/scgodbold/meal-planner/api/util/token"
)

func Refresh(c *gin.Context) {
	tokenString := token.TokenStringFromContext(c)

	// Is our token valid?
	if err := token.TokenValid(tokenString); err != nil {
        controllers.SendErrorResponse(c, http.StatusUnauthorized, nil)
		return
	}

	// Fetch current user id
	uid, err := token.ExtractTokenID(tokenString)
	if err != nil {
        controllers.SendErrorResponse(c, http.StatusUnauthorized, nil)
		return
	}

	// Fetch user information
	user, err := user.FetchUserByUid(uid)
	if err != nil {
        controllers.SendErrorResponse(c, http.StatusUnauthorized, nil)
		return
	}

	// Create new token for user
	newToken, err := token.GenerateToken(uid)
	if err != nil {
		// Dunno what went wrong but just tell em to sign back in
        controllers.SendErrorResponse(c, http.StatusUnauthorized, nil)
		return
	}

	c.JSON(http.StatusOK, gin.H{"username": user.Username, "token": newToken})
}
