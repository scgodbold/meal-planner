package auth

import (
	"html"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/scgodbold/meal-planner/api/controllers"
	"gitlab.com/scgodbold/meal-planner/api/models"
)

type LoginInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type LoginError struct {
    msg string
}

func (l *LoginError) Error() string {
    return l.msg
}

func Login(c *gin.Context) {
	var input LoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, err)
		return
	}

	u := models.User{
		Username: html.EscapeString(strings.TrimSpace(input.Username)),
	}
	token, err := u.Authenticate(input.Password)
	if err != nil {
        controllers.SendErrorResponse(c, http.StatusBadRequest, &LoginError{"username or password were incorrect"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"username": u.Username, "token": token})
}
