package controllers

import "github.com/gin-gonic/gin"

type ErrorResponse struct {
    Code int
    Status string
    Message string
}

type SuccessResponse struct {
    Status string
    Data interface{}
}

func SendErrorResponse(c *gin.Context, status int, err error) {
    var msg string
    if err == nil {
        msg = ""
    } else {
        msg = err.Error()
    }
    resp := ErrorResponse{
        Code: status,
        Status: "error",
        Message: msg,
    }
    c.JSON(status, resp)
}

func SendSuccessResponse(c *gin.Context, status int, data interface{}) {
    resp := SuccessResponse{
        Status: "ok",
        Data: data,
    }
    c.JSON(status, resp)
}
