package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/scgodbold/meal-planner/api/models"
	"gitlab.com/scgodbold/meal-planner/api/routes"
)

func main() {
	// Load env
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Open DB Connection
	models.ConnectDatabase()
	defer models.CloseDatabse()

	// Load router
	router := routes.GetRouter()

	// Run application
	if bindUri, ok := os.LookupEnv("MP_BIND_URI"); !ok {
		router.Run()
	} else {
		router.Run(bindUri)
	}
}
