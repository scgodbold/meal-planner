package models

import (
	"gitlab.com/scgodbold/meal-planner/api/util/token"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID       uint   `storm:"id,increment"`
	Username string `storm:"unique"`
	Password string
}

func (u *User) Save() (*User, error) {
	err := DB.Save(u)
	return u, err
}

func (u *User) Exists() bool {
	var user User
	err := DB.One("Username", u.Username, &user)
	if err != nil {
		return false
	}
	return true
}

func (u *User) Authenticate(password string) (string, error) {
	var err error
	// Existing user entry in DB to compare against
	var record User

	err = DB.One("Username", u.Username, &record)
	if err != nil {
		return "", err
	}

	// Check password
	err = bcrypt.CompareHashAndPassword([]byte(record.Password), []byte(password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}

	// Generate jwt
	token, err := token.GenerateToken(record.ID)
	if err != nil {
		return "", err
	}

	return token, nil
}

