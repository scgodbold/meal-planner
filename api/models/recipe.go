package models

type Recipe struct {
    ID          uint        `storm:"id,increment"`
    Owner       uint        `strom:"index"`
    Title       string
    Steps       []string
    Ingredients []Ingredient
    Tags        []string
}

type Ingredient struct {
    Name        string
    Amount      float32
    Unit        string
}

func (r *Recipe) Save() (*Recipe, error) {
    err := DB.Save(r)
    return r, err
}

func (r *Recipe) Delete() error {
    return DB.DeleteStruct(r)
}

func (r *Recipe) Update() error {
    return DB.Update(r)
}
