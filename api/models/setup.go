package models

import (
	"log"
	"os"

	"github.com/asdine/storm/v3"
)

var DB *storm.DB

func ConnectDatabase() {
	var err error
	dbPath := os.Getenv("MP_DBPATH")
	DB, err = storm.Open(dbPath)
	if err != nil {
		log.Fatalf("Unable to open connection to database: %v", err.Error())
	}
}

func CloseDatabse() {
	DB.Close()
}
