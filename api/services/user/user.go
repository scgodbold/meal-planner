package user

import (
	"html"
	"strings"

	"gitlab.com/scgodbold/meal-planner/api/models"
	"golang.org/x/crypto/bcrypt"
)

type UserError struct {
    msg string
}

func (e *UserError) Error() string {
    return e.msg
}


func FetchUserByUid(uid uint) (*models.User, error) {
	var record models.User
	err := models.DB.One("ID", uid, &record)
	if err != nil {
		return nil, err
	}
	return &record, nil
}

func Register(username, password string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
        return &UserError{msg: "Username is already taken"}
	}
    u := &models.User{
        Username: html.EscapeString(strings.TrimSpace(username)),
        Password: string(hashedPassword),
    }
	if u.Exists() {
        return &UserError{msg: "Username is already taken"}
	}
	// Save our user
    u.Save()
    return nil
}
