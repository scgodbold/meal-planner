package recipe

import (
	"gitlab.com/scgodbold/meal-planner/api/models"
)


func FetchByOwner(owner uint) ([]models.Recipe, error) {
    var records []models.Recipe
    err := models.DB.Find("Owner", owner, &records)
    return records, err
}

func FetchById(id uint) (models.Recipe, error) {
    var record models.Recipe
    err := models.DB.One("ID", id, &record)
    return record, err
}

func Create(owner uint, title string, steps []string, ingredients []models.Ingredient, tags []string) (*models.Recipe, error){
    r := &models.Recipe{
        Owner: owner,
        Title: title,
        Steps: steps,
        Ingredients: ingredients,
        Tags: tags,
    }
    return r.Save()
}

func CreateIngredient(name string, amount float32, unit string) (models.Ingredient, error){
    // TODO: Make sure unit is real
    return models.Ingredient{
        Name: name,
        Amount: amount,
        Unit: unit,
    }, nil
}

func Delete(id uint) error {
    r, err := FetchById(id)
    if err != nil {
        return err
    }
    return r.Delete()
}

func Update(id uint, title string, steps []string, ingredients []models.Ingredient, tags[]string) error {
    r := &models.Recipe{
        ID: id,
        Title: title,
        Steps: steps,
        Ingredients: ingredients,
        Tags: tags,
    }
    return r.Update()
}
